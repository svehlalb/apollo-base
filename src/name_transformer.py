import re


def str_to_utf8(sentence: str) -> str:
    """
    Temporary function for transforming text to ASCI characters
    Can be deleted once the bug with pdfLaTeX compiler is solved
    (possible solution is to change the compiler to more advanced XeLaTex
    in SDAPS source code)

    :param sentence: str
        String of text to be transformed
    :return: str
        Transformed text
    """
    sentence = re.sub('[Á]', 'A', sentence)
    sentence = re.sub('[á]', 'a', sentence)
    sentence = re.sub('[é]', 'e', sentence)
    sentence = re.sub('[ě]', 'e', sentence)
    sentence = re.sub('[É]', 'E', sentence)
    sentence = re.sub('[Ě]', 'E', sentence)
    sentence = re.sub('[í]', 'i', sentence)
    sentence = re.sub('[Í]', 'I', sentence)
    sentence = re.sub('[ý]', 'y', sentence)
    sentence = re.sub('[Ý]', 'y', sentence)
    sentence = re.sub('[ó]', 'o', sentence)
    sentence = re.sub('[Ó]', 'O', sentence)
    sentence = re.sub('[ú]', 'u', sentence)
    sentence = re.sub('[Ú]', 'U', sentence)
    sentence = re.sub('[ů]', 'u', sentence)
    sentence = re.sub('[Č]', 'C', sentence)
    sentence = re.sub('[č]', 'c', sentence)
    sentence = re.sub('[Ď]', 'D', sentence)
    sentence = re.sub('[ď]', 'd', sentence)
    sentence = re.sub('[Ň]', 'N', sentence)
    sentence = re.sub('[ň]', 'n', sentence)
    sentence = re.sub('[Ř]', 'R', sentence)
    sentence = re.sub('[ř]', 'r', sentence)
    sentence = re.sub('[Š]', 'S', sentence)
    sentence = re.sub('[š]', 's', sentence)
    sentence = re.sub('[Ť]', 'T', sentence)
    sentence = re.sub('[ť]', 't', sentence)
    sentence = re.sub('[Ž]', 'Z', sentence)
    sentence = re.sub('[ž]', 'z', sentence)
    sentence = re.sub('[%]', '\%', sentence)
    return sentence
