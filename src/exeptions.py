class ExamExistsError(Exception):
    """
        ExamExistsError is called if there is an attempt to override existing
        exam folder
    """

    def __init__(self, exam_id: int, message="Exam with given ID already exists"):
        """
        Function constructs exception

        :param exam_id: int
            identifies existing exam
        :param message: str
            contains custom error message
        """
        self.exam_id = exam_id
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'ID: {self.exam_id} -> {self.message}'


class InvalidExamPropertiesError(Exception):
    """
    InvalidExamPropertiesError is called if the properties of uploaded files are invalid.
    """
    def __init__(self, exam_id, message="Given file results are not compatible with currently stored exam", files=[]):
        """
        Function constructs exception

        :param exam_id: int
            identifies associated exam
        :param message: str
            contains custom error message
        :param files: file
            contains invalid files
        """
        self.exam_id = exam_id
        self.message = message
        self.files = files
        super().__init__(self.message)

    def __str__(self):
        return f'ID: {self.exam_id} -> {self.message} for {self.files}'
