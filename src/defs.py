from enum import Enum


class ExamType(Enum):
    """
    Class ExamType is representing two possible exam types:
        - SINGLE_VARIANT ... exam has only one variant generated
        - MULTI_VARIANT ... exam has multiple variants generated
    """
    SINGLE_VARIANT = 1
    MULTI_VARIANT = 2
