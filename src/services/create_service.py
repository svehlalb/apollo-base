import traceback

from flask_restful import Resource

from src.name_transformer import str_to_utf8
from src.services.SDAPS_controller import SDAPSController
from src.services.editor_service import EditorService
from src.services.exam_manager_service import ExamManagerService
import os
import shutil
import sys


class CreateService(Resource):
    """
        Service is responsible for creating exam folder. It uses EditorService for creating tex file and
        at the end of preparation process it calls SDAPSController for generating PDF files.

        By default its attributes are private.
        exam_id: int
        editor: EditorService
        exam_path: str

    """
    __exam_id = -1
    __exam_path = None

    def __init__(self, exam_id: int):
        """
        Constructor function
        :param exam_id: int
            exam identifier
        """
        path = ExamManagerService.get_exam_path(exam_id)
        # currently if exam already exists, it is overwritten
        if path is not None:
            shutil.rmtree(path)
        self.editor = EditorService()
        self.exam_id = exam_id
        self.exam_path = "./src/data/exam_" + str(self.exam_id) + '/variant_'

    def create_exam(self, content: list, students: list, metadata: dict):
        """
        Function responsible for creating folders for exam and its variants,
        using EditorService and calling SDAPSController

        :param content: list
            list of question with answers divided into correct and incorrect ones
        :param students: list
            list of students assigned to exam variants
        :param metadata: dict
            dictionary that represents additional exam information
        """

        # check if the content is not empty
        if len(content) < 1:
            raise IOError

        try:
            os.mkdir("./src/data/exam_" + str(self.exam_id))
        except OSError:
            print("Creation of the directory %s failed" % self.exam_path)
        else:
            print("Successfully created the directory %s " % self.exam_path)
        try:
            for index, variant in enumerate(content):
                self.editor.create_tex(variant, metadata)
                self.extract_students(students[index])
                SDAPSController.setup_sdaps(index + 1, self.exam_path)

                # move generated examstore file used for saving data
                # about questions and answers for future evaluation
                shutil.move('./src/data/examstore.txt', self.exam_path + str(index + 1) + '/examstore.txt')

        except Exception as e:
            print("Error while creating a tex file")
            print(e)
            traceback.print_exception(*sys.exc_info())
            raise IOError

    def extract_students(self, students: list):
        """
            Function is used for extracting data about students and save them into separate file
            that is later used for unique identification of each exam by QR code

        :param students: list
            List of students for extraction
        """
        data = []

        # temporary file path, not safe if called from multiple users in same time, need changes
        txt_path = './src/data/students.txt'
        if os.path.exists(txt_path):
            os.remove(txt_path)

        for student in students:
            data.append(str_to_utf8(student['firstname']) + ' ' + str_to_utf8(student['lastname']) + ':  ' + student[
                'username'] + '\n')

        with open(txt_path, 'w') as file:
            file.writelines(data)
