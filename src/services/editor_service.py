import random
from src.name_transformer import str_to_utf8


class EditorService:
    """
        Editor service is responsible for creating tex file and storing data about exam question and answer order
        for process of evaluation.
    """

    def __init__(self):
        """
            Initial set up
        """
        self.path = "./src/data/exam"
        self.variant = []
        self.metadata = {}

    def create_tex(self, variant: list, metadata: dict):
        """
        Function sets up variables and initiates creation of tex and store file
        :param variant: list
            list of questions and answers of current exam variant
        :param metadata: dict
            dictionary that represents additional exam information
        """
        self.variant = variant
        self.metadata = metadata
        self.set_up_tex()
        self.fill_tex()

    def set_up_tex(self):
        """
        Function sets up tex file and its header that is predefined for every tex file
        """

        # defined header according to SDAPS LaTeX class standard
        data = str_to_utf8('\\documentclass[czech,pagemark,oneside,stamp,print_questionnaire_id]{'
                           'sdapsclassic}\n\\usepackage[utf8]{inputenc}\n\\usepackage{tikz}\n\\usepackage{'
                           'multicol}\n\\title{' + self.metadata['name'] + '}\n\\author{' + self.metadata[
                               'subject'] + '}\n\\begin{document}\n\\normalsize')

        # creation of tex file
        with open(self.path + '.tex', 'w') as file:
            file.writelines(data)

    def fill_tex(self):
        """
        Function is responsible for filling the content of tex file
        :return:
        """

        # getting the content of tex file
        with open(self.path + '.tex', 'r') as file:
            tex_data = file.readlines()
        tex_data.append('\\begin{questionnaire}\n')
        tex_data.append('\\begin{info}\n' + str_to_utf8(self.metadata['description']) + '\n\\end{info}\n')
        store_data = []
        for item in self.variant:
            # questions and pages are stored together, when page is identified
            # \newpage command is added to tex file
            if item['type'] == 'page':
                if item['id'] != 1:
                    tex_data.append('\\newpage\n')
                    store_data.append('\n')
                tex_data.append('\\section{Sekce ' + str(item['id']) + '}\n')
                continue
            else:
                # if page is not identified, item is question
                tex_data, store_data = self.fill_question(item, tex_data, store_data)

        # ending tex file
        tex_data.append('\\end{questionnaire}\n\\end{document}\n')

        # final version of tex file is created
        with open(self.path + '.tex', 'w') as file:
            file.writelines(tex_data)

        # examstore.txt file for storing data about exam is created
        with open(self.path + 'store.txt', 'w') as file:
            file.writelines(store_data)

    def fill_question(self, question: dict, tex_data: list, store_data: list) -> tuple:
        """
        Function is responsible for filling tex file with received question and appended answers
        It also stores data for binary store file which is used later in evaluation
        :param question: dict
            processed question with answers
        :param tex_data: list
            data to be written in tex file
        :param store_data: list
            data to be written in binary store file
        :return:
            tuple of edited tex_data: str, store_data: str
        """

        # appending question text for tex file
        tex_data.append(str_to_utf8('\\begin{choicequestion}{' + question['question'] + '}\n'))

        # appending question id for binary store file
        store_data.append(str(question['id']) + ':')

        # dependent on received question data format – treat carefully
        correct_answers = [question['correct_answer']]
        incorrect_answers = []
        incorrect_answers.extend(question['incorrect_answers'])

        # makes order of answers random - different of received order
        correct_index = random.randint(0, 3)
        print(correct_index)
        for i in range(0, len(correct_answers) + len(incorrect_answers)):
            # true if there is no incorrect answer left or if there is a correct answer
            # and random int equals index
            if (i == correct_index and len(correct_answers) != 0) or 0 == len(incorrect_answers):
                tex_data.append(str_to_utf8('\\choiceitem{' + correct_answers[0] + "}\n"))
                store_data.append(str(i) + '[' + str(question['points']) + ']')
                correct_answers.pop(0)
            else:
                tex_data.append(str_to_utf8('\\choiceitem{' + incorrect_answers[0] + "}\n"))
                incorrect_answers.pop(0)

        # appending end of question
        store_data.append(';')
        tex_data.append('\\end{choicequestion}\n')

        return tex_data, store_data
