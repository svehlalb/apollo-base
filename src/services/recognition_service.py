from src.defs import ExamType
from src.exeptions import InvalidExamPropertiesError
from src.services.SDAPS_controller import SDAPSController
from src.services.exam_manager_service import ExamManagerService


class RecognitionService:
    """
    RecognitionService class is used for checking uploaded files and calling SDAPSController
    to add them into the exam folder and run recognition process.
    """
    file_path = './src/data/'

    def __init__(self, exam_id: int, files: list):
        """
        Initial constructor

        :param exam_id: int
            exam identifier
        :param files: list
            list of received files for recognition
        """
        self.exam_id = exam_id
        self.files = files
        self.exam_type = ExamManagerService.get_exam_type(self.exam_id)
        self.variant_path = ExamManagerService.get_exam_variant_path(self.exam_id)

    def add_scanned_files(self):
        """
        Function checks received files and calls SDAPSController for initiating recognition process
        """
        # condition checks if the number of received files is matching the exam type:
        #   single = one file, one variant
        #   multiple = more than one file, more than one variant
        if (self.exam_type is None) or (self.exam_type is ExamType.MULTI_VARIANT and len(self.files) <= 1) or (
                self.exam_type is ExamType.SINGLE_VARIANT and len(self.files) > 1):
            raise InvalidExamPropertiesError(self.exam_id, self.files)
        try:
            # if the exam has only one variant, index is default 1
            if self.exam_type == ExamType.SINGLE_VARIANT:
                SDAPSController.recognize_sdaps(self.files[0].filename, self.variant_path, self.file_path)
            else:
                # current approach requires correct name of file identified with variant index id
                # in format "filename_XX.pdf where XX = variant_id
                for file in self.files:
                    SDAPSController.recognize_sdaps(file.filename, self.variant_path, self.file_path,
                                                    ExamManagerService.extract_index(file.filename))
        except Exception as e:
            print(e)
            raise OSError
