import subprocess


class SDAPSController:
    """
    SDAPSController is used as interface for SDAPS program for other server services.
    It uses WSL 2 for colling commands defined by SDAPS.
    """
    @staticmethod
    def setup_sdaps(index: int, variant_path: str):
        """
        Function calls commands to create exam folder, fill it with PDF file and match exam sheets with
        given students

        :param index: int
            identifier of variant
        :param variant_path: str
            variant folder path
        :return:
        """
        subprocess.check_call(['wsl', 'sdaps', 'setup', variant_path + str(index), './src/data/exam.tex'])
        subprocess.check_call(
            ['wsl', 'sdaps', 'stamp', '-f', './src/data/students.txt', variant_path + str(index)])

    @staticmethod
    def recognize_sdaps(file_name: str, variant_path: str, file_path: str, index=1):
        """
        Function calls commands to add scans, to reorder them and to recognize answers

        :param file_name: str
            file name to add
        :param variant_path: str
            variant folder where the file should be added to
        :param file_path: str
            path to the file
        :param index: int
            variant identifier, if not set default value is 1 for SINGLE type exam
        """
        subprocess.check_call(
            ['wsl', 'sdaps', 'add', '--convert', variant_path + str(index), file_path
             + file_name])
        subprocess.check_call(
            ['wsl', 'sdaps', 'recognize', '--identify', variant_path + str(index)])
        subprocess.check_call(
            ['wsl', 'sdaps', 'reorder', variant_path + str(index)])
        subprocess.check_call(
            ['wsl', 'sdaps', 'recognize', variant_path + str(index)])

    @staticmethod
    def make_csv(path: str, index=1):
        """
        Function for exporting data about filled answers to csv file

        :param path: int
            exam variant path
        :param index: int
            variant identifier, if not set default value is 1 for SINGLE type exam
        """
        subprocess.check_call(
            ['wsl', 'sdaps', 'csv', 'export', path + str(index)])
