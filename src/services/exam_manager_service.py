import os
import re

from src.defs import ExamType


class ExamManagerService:
    """
        Class ExamManagerService contains static methods for managing operations associated
        with exam folders
    """

    @staticmethod
    def get_exam_path(exam_id: int) -> str or None:
        """
        Function is used for getting exam folder path

        :param exam_id: int
            exam identifier
        :return: str or None
            str if the exam path is found otherwise None
        """
        for file in os.listdir("./src/data"):
            if file == "exam_" + str(exam_id):
                return "./src/data/" + file
        return None

    @staticmethod
    def get_pdf(exam_id: int, variant_id=1) -> str or None:
        """
        Function is used for getting generated exam PDF files

        :param exam_id: int
            exam identifier
        :param variant_id: int
            exam variant identifier
        :return: str or None
             str if the PDF file is found otherwise None
        """
        path = ExamManagerService.get_exam_path(exam_id)
        if path is None:
            raise FileNotFoundError

        return path + '/variant_' + str(variant_id) + '/', 'stamped_1.pdf'

    @staticmethod
    def get_exam_type(exam_id: int) -> ExamType or None:
        """
        Function is used for getting the type of exam defined by ExamType Enum class.

        :param exam_id: int
            exam identifier
        :return: ExamType or None
            ExamType if the exam exists otherwise None
        """
        path = ExamManagerService.get_exam_path(exam_id)
        if path is None:
            return None
        for file in os.listdir(path):
            if file == "variant_2":
                return ExamType.MULTI_VARIANT
        return ExamType.SINGLE_VARIANT

    @staticmethod
    def validate_result_file_name(file_name: str):
        """
        Currently only for checking suffix of uploaded file.
        In further implementation it should cover more file types.

        :param file_name: str
            name of given file
        """
        if file_name.find('.pdf') == -1:
            raise IOError

    @staticmethod
    def get_exam_variant_path(exam_id: int) -> str:
        """
        Function returns specific exam variant folder path.

        :param exam_id: int
            exam identifier
        :return: str
            exam variant folder path
        """
        return './src/data/exam_' + str(exam_id) + '/variant_'

    @staticmethod
    def extract_index(filename: str) -> int:
        """
        Function extracts index from file name.
        In future implementation this function should be replaced for automatic
        variant detection from QR codes

        :param filename: str
            name of the uploaded file
        :return: int
            extracted index
        """
        return int(re.search(r"\_([0-9]+)\.", filename).group(1))
