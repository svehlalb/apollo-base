import csv
import json
import os
import re

from src.defs import ExamType
from src.exeptions import InvalidExamPropertiesError
from src.services.SDAPS_controller import SDAPSController
from src.services.exam_manager_service import ExamManagerService


class ResultService:
    """
    ResultService class is responsible for getting results of exam and evaluating them using examstore.txt file
    for comparison. It then create list of question and student's answers that are later send to client.
    """
    def __init__(self, exam_id: int):
        """
        Initial constructor

        :param exam_id: id
            exam identifier
        """
        self.exam_id = exam_id
        self.exam_type = ExamManagerService.get_exam_type(self.exam_id)
        self.results = []
        self.correct_answers = []
        self.variant_path = ExamManagerService.get_exam_variant_path(self.exam_id)

    def get(self) -> str:
        """
        Function initiates evaluation process and returns results of this process.
        :return: str
            results of evaluation process in JSON format
        """
        if self.exam_type is None:
            raise InvalidExamPropertiesError(self.exam_id)
        try:
            variants = os.listdir("./src/data/exam_" + str(self.exam_id))
            # evaluating every variant of exam from index 1 - n where n is the last index
            for index in range(1, len(variants) + 1):
                SDAPSController.make_csv(self.variant_path, index)
                self.evaluate_results(index)
        except Exception as e:
            print(e)
            raise OSError

        return json.dumps(self.results)

    def evaluate_results(self, index: int):
        """
        Function is used for evaluation one variant of exam. It extracts data from examstore.txt
        and

        :param index:
        :return:
        """
        with open(self.variant_path + str(index) + '/examstore.txt', 'r') as file:
            self.correct_answers = file.readlines()
        student_answers = []
        with open(self.variant_path + str(index) + '/data_1.csv', 'r') as file:
            reader = csv.DictReader(file)
            # every row represents one student's exam answer sheet
            for row in reader:
                student_answers.append(row)

        # comparing answers of each student
        for student_test in student_answers:
            self.compare_answers(student_test, variant=index)

    def compare_answers(self, student_test: dict, variant: int):
        """
        Function compares answers of one student's answer sheet

        :param student_test: dict
            Dictionary containing data about students answers
        :param variant: int
            variant identifier
        """

        # split string with : and take second value from format NameSurname:username
        student_result = {'username': student_test['questionnaire_id'].split(':', 1)[1],
                          'questions': [],
                          'totalPoints': 0,
                          'maximumPoints': 0,
                          'variant': chr(ord('A') + variant - 1)}

        for section_index, section in enumerate(self.correct_answers, 1):
            # split the correct answers into list individually
            individual_correct_answers = section.split(';')
            for question_index, question in enumerate(individual_correct_answers, 1):
                if question is '\n' or question is '':
                    break
                question_id, correct_answer = question.split(':')
                max_points = int(re.search(r"\[([0-9]+)\]", correct_answer).group(1))
                answer_index = int(correct_answer[0]) + 1
                points = 0
                filled_answer_index = 0
                # only for PoC purposes, one answer is correct with maximum of points
                if student_test[f'{section_index}_{question_index}_{answer_index}'] == '1':
                    points = max_points
                    filled_answer_index = answer_index

                # searching for filled answer, for PoC purposes only one answer is correct
                if filled_answer_index == 0:
                    for index in range(1, 10):
                        if f'{section_index}_{question_index}_{index}' not in student_test:
                            break

                        if student_test[f'{section_index}_{question_index}_{index}'] == '1':
                            filled_answer_index = index
                            break

                student_result['questions'].append({'questionId': int(question_id),
                                                    'questionIndex': f'{section_index}.{question_index}',
                                                    'points': points,
                                                    'maxPoints': max_points,
                                                    'filledAnswer': chr(ord('a') + filled_answer_index - 1),
                                                    'correctAnswer': chr(ord('a') + answer_index - 1)
                                                    })
                student_result['totalPoints'] = int(student_result['totalPoints']) + points
                student_result['maximumPoints'] = int(student_result['maximumPoints']) + max_points

        self.results.append(student_result)

