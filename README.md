# Server
Welcome to Server side of web application created for bachelor theses.
Server takes care of computing operations associated with generating 
OMR readable exam sheets and its automated evaluation.

More details about Server functions and processes are described in folowing 
chapters.

Whole repository is also available on GitLab https://gitlab.fit.cvut.cz/svehlalb/apollo-base


## Requirements
For successful installation you need to first make sure your
computer fulfils following requirements:

  * Operating system Windows 10  
  * Extensions installed: 
    * WSL 2 – [Installation guide][https://docs.microsoft.com/en-us/windows/wsl/install-win10]
      * SDAPS – install a program SDAPS in WSL 2 command line, follow the same steps
        as described [here] for linux distributions 
    * Python 3.9 – [Installation guide][https://phoenixnap.com/kb/how-to-install-python-3-windows]
    * Flask – [Installation guide][https://pypi.org/project/Flask/]
      * With Flask install following extensions: flask_restful, flask_cors
    
The installation process isn't optimized so if you run into any problems
contact Albert Svehla via this email address: svehlab@cvut.cz

## Run
Steps for successful running of Server:

1. In a directory 'server' double-click on file run.bat.
2. Application should automatically run itself. If the process didn't end successfully 
   check that you have installed all required extensions.   
3. Server runs at address http://127.0.0.1:5000/, it is used only for 
   API endpoints so no GUI will be seen through web browser.
   

## Usability
As user, you can control server only via API. Valuable information are stored in data folder
where you can browse through created exams and generated PDF exam sheets.

## Processes
### Creating exam
Creating exam process starts with client calling POST /exam endpoint.
Server then proceeds:
1. Server calls CreateService that establishes folders and calls EditorService
2. EditorService creates tex file filled with questions and answers. 
   It also creates *examstore.txt* file that is used for storing data about 
   correct answers for evaluation process.
3. Once the EditorService is done, CreateService calls SDAPS Controller that 
   initiates process in SDAPS which creates exam folder and PDF files.
4. Client the asks for PDF files through GET /exam endpoint

### Evaluating exam
Evaluating exam process starts with client calling POST /results/<exam_id> endpoint.
Server then proceeds:
1. Server calls Recognition service, it checks uploaded files and calls SDAPSController 
   which initiates SDAPS recognition process
2. Once the Client asks for results via GET /results/<exam_id> server calls
   ResultService which extracts data from *examstore.txt* and compares it with
   data from csv file that has been generated via SDAPSController
3. Once the comparison(evaluation) process is complete, server sends results to client.  

##  Structure
Application files are organized in following structure:   

````
├── src  
     └── data ... folder with stored data 
                  containing created exams and evaluation
     └── services ... folder responsible for operating program SDAPS and
                      other necessary operations associated with the processes     
├── app.py 
        

````

### Data storage
Data folder is based on following hierarchy:

````
├── data 
     └── exam_[exam_id]
          └── variant_[variant_id]

````

Folder variant is generated automatically by SDAPS.

### API endpoints
Server's 'front gate' is file app.py that contains functions used as endpoints for server-side API.<br> It operates 4 different endpoints:
````    
    - POST /exam .. for posting data to construct exam files
       
        requested data example {
          examId: int,
          content: [
                    [.. first variant ..
                       0:Question
                            id:1
                            type:"page"
                       1:Question
                            correct_answer:"troposféra"
                            id:5
                            incorrect_answers:
                                0:"stratosféra"
                                1:"tropopauza"
                                2:"termosféra"
                            points:1
                            question:"Ve které vrstvě atmosféry 
                            dochází nejčastěji k meteorologickým 
                            jevům?"
                            type:"multiple"
                    ],
                    [.. second variant ..]
                   ],
          students: [ 
                      [ .. first variant ..
                      0:Student
                          email:"jikla@example.com"
                          firstname:"Jiří"
                          lastname:"Klapal"
                          username:"jikla"
                      ],
                      [.. second variant ..]    
                    ]
          metadata: {
                     description:"Popis"
                     id:155
                     layout:"TestOnly"  ... exam form currently only one  
                     name:"Nazev testu"
                     studentGroup:"all"
                     subject:"Bi-DBS"
                     variants:2
                    }
        }
        
    - GET  /exam .. for getting exam PDF files
        - sends PDF files

    - POST /results/<exam_id> .. for posting scanned files for recognition
        - requests PDF files
        
    - GET /results/<exam_id> .. for getting exam results after exam evaluation
         posted data example 
         [
            'username': 'jikla' 
            'questions': [
                          {.. first question ..
                             'questionId': 13,
                             'questionIndex': 1,
                             'points': 0, ... given points for answer
                             'maxPoints': 4,
                             'filledAnswer': a, ... for manual correction
                             'correctAnswer': c ... for manual correction
                          },
                          {.. second question ..}                            
                          ],
            'totalPoints': 0,
            'maximumPoints': 10,
            'variant': A 
         },
         {.. second student ..}
         ]
````


[https://docs.microsoft.com/en-us/windows/wsl/install-win10]: https://docs.microsoft.com/en-us/windows/wsl/install-win10

[https://phoenixnap.com/kb/how-to-install-python-3-windows]: https://phoenixnap.com/kb/how-to-install-python-3-windows

[https://pypi.org/project/Flask/]: https://pypi.org/project/Flask/

[here]: https://sdaps.org/install/