"""
app.py contains functions used as endpoints for server-side API
it operates 4 different endpoints:
    - POST /exam .. for posting data to construct exam files
    - GET  /exam .. for getting exam PDF files

    - POST /results/<exam_id> .. for posting scanned files for recognition
    - GET /results/<exam_id> .. for getting exam results after exam evaluation
"""

from flask import Flask, request, send_from_directory
from flask_cors import CORS, cross_origin
from flask_restful import reqparse
import json

from src.exeptions import ExamExistsError
from src.services.create_service import CreateService
from src.services.exam_manager_service import ExamManagerService
from src.services.recognition_service import RecognitionService
from src.services.result_service import ResultService

app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, resources={r"/exam/": {"origins": "http://localhost:port"}})


@app.route('/exam', methods=['POST'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def create_project():
    """
    Function is responsible for operating POST /exam API endpoint
    It extracts data from parameters and initiates exam folder creation process

    :return: str
        function returns informative string if it succeeded or not
    """
    parser = reqparse.RequestParser()
    try:
        # examId: int Exam unique identifier
        parser.add_argument("examId")
        # content: list List of questions and answers
        parser.add_argument("content")
        # students: list List of students assigned to exam
        parser.add_argument("students")
        # metadata: Object Object that contains name of course, name, description etc.
        parser.add_argument("metadata")

        args = parser.parse_args()
        exam_id = int(args['examId'])
        content = json.loads(args['content'])
        students = json.loads(args['students'])
        metadata = json.loads(args['metadata'])

        exam = CreateService(exam_id)
        exam.create_exam(content=content,
                         students=students,
                         metadata=metadata)

        return json.dumps("Exams created successfully", indent=4), 201

    except ExamExistsError as e:
        return json.dumps(e.message), 409

    except Exception as e:
        print(e)
        return json.dumps("Unspecified error"), 404


@app.route('/exam', methods=['GET'])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def get_project():
    """
    Function is responsible for operating GET /exam API endpoint
    It extracts data from parameters and return generated PDF files

    :return: file
        If the PDF files are matching the searching criteria they are returned
    """

    parser = reqparse.RequestParser()
    try:
        # variantId: int identifier of exam variant
        parser.add_argument("variantId")
        # examId: int identifier of exam
        parser.add_argument("examId")

        args = parser.parse_args()
        variant_id = int(args['variantId'])
        exam_id = int(args['examId'])

        file_path, file_name = ExamManagerService.get_pdf(int(exam_id), int(variant_id))
        return send_from_directory(file_path, filename=file_name, as_attachment=True)
    except Exception as e:
        print(e)
        return json.dumps("Unspecified error" + str(e)), 404


@app.route("/results/<exam_id>", methods=["POST"])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def create_results(exam_id):
    """
    Function is responsible for operating POST /results/<exam_id> API endpoint
    It extracts data from endpoint url and parameter. It initiates exam recognition process

    :param exam_id: int
        exam identifier
    :return: str
        function returns informative string if it succeeded or not
    """
    i = 0
    uploaded_files = []
    try:
        # getting all sent files
        while request.files.get('files[' + str(i) + ']'):
            file = request.files.get('files[' + str(i) + ']')
            # validating files
            ExamManagerService.validate_result_file_name(file.filename)
            file.save('./src/data/' + file.filename)

            uploaded_files.append(file)
            i += 1

        recognition = RecognitionService(exam_id, uploaded_files)
        recognition.add_scanned_files()
        return "Results added and recognized successfully", 201
    except Exception as e:
        return "Unspecified error" + str(e), 404


@app.route("/results/<exam_id>", methods=["GET"])
@cross_origin(origin='localhost', headers=['Content- Type', 'Authorization'])
def get_results(exam_id):
    """
    Function is responsible for operating GET /results/<exam_id> API endpoint
    It extract data from parameters and returns evaluated exam results

    :param exam_id: int
        exam identifier
    :return: list
        If the exam is matching the searching criteria function returns list of results
    """
    try:
        results = ResultService(exam_id)
        results_list = results.get()
        return json.dumps(results_list)
    except Exception as e:
        print(e)
        return json.dumps("Following error has occurred" + str(e)), 404


if __name__ == '__main__':
    app.run(debug=True)
